#!/bin/sh
echo "Install ext received version $1"

# Check if the number is 8.0 or greater
if awk -v ver="$1" 'BEGIN {if (ver >= 8.0) exit 0; else exit 1}'; then
  # Run the command for numbers 8.0 or greater
  echo "php >= 8, installing otel"
  # Place your command here
  install-php-extensions gd pdo_mysql zip redis pcntl rdkafka opentelemetry grpc mongodb xdebug @composer
else
  # Run the command for numbers less than 8.0
  echo "php < 8, not installing otel"
  # Place your command here
  install-php-extensions gd pdo_mysql zip redis pcntl rdkafka mongodb xdebug @composer
fi
